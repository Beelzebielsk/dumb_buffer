This demo of buffer.
The buffer consists of 4 regions, each 8 switches (bits) wide, for a
total of 32 switches (bits).

Recommended setup for using the buffer:
The first eight switches are data switches. Used to input data.
Last two switches are selector. They choose between four different
regions, 1 2 3 4 (show them). The rightmost pushbutton is for writing to
the buffer.
And for demonstration purposes, the eight red LEDs above the switches
show the state of the current buffer region.

To demonstrate, I'll write 1, 2, 3, 4 in binary to each region, then
cycle through them.

Change the data switches to the configuration you want, and when
satisfied, hit the write button. 
