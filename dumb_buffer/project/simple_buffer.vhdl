Library ieee;
use ieee.std_logic_1164.all;

entity simple_buffer is
	generic(
		button_up_is_one : boolean := true
		-- Controls whether or not to negate the write signal.
		-- Write signal should work on button press, not on button release.
		-- If it doesn't, then switching regions will count as a write, since
		-- the button being up counts as the clock right after an active edge
		-- and thus switching to a new region will look like a clock edge
		-- for each buffer.
	);
	port(
		write_signal     : in std_logic;
		-- Controls writing to the current eight bit region.
		data             : in std_logic_vector(7 downto 0);
		-- Data to write to the current eight bit region.
		region_selectors : in std_logic_vector(1 downto 0);
		-- Selects eight bit region. Treat selectors as binary:
		-- - 00: 0th eight bit region (bits 0 through 7)
		-- - 01: 1th eight bit region (bits 8 through 15)
		-- - 10: 2th eight bit region (bits 16 through 23)
		-- - 11: 3th eight bit region (bits 24 through 31)
		q                : out std_logic_vector(31 downto 0)
		-- The full output of the buffer. 32 bits wide (since there's four
		-- eight bit regions, 8*4=32 bits wide.
	);
end entity simple_buffer;

architecture arch of simple_buffer is

	component eight_bit_buffer
		port(
			clock : IN STD_LOGIC ;
			data  : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
			load  : IN STD_LOGIC ;
			q     : OUT STD_LOGIC_VECTOR (7 DOWNTO 0)
		);
	end component;

	component demux
		PORT (
			input     : in std_logic;
			selectors : in std_logic_vector(1 downto 0);
			outputs   : OUT STD_LOGIC_VECTOR (3 DOWNTO 0)
		);
	end component;

	-- This is the signal that goes to the write signal demux. It is
	-- controlled by the input write signal, but altered by the generic
	-- 'button_up_is_one' such that the button's resting position makes
	-- this signal '0'.
	signal actual_write_signal : std_logic := '0';

	-- Output from the write signal demux.
	signal write_signals : std_logic_vector(3 downto 0);
	alias register_output_0 : 
		std_logic_vector(7 downto 0) is q(7 downto 0);
	alias register_output_1 : 
		std_logic_vector(15 downto 8) is q(15 downto 8);
	alias register_output_2 : 
		std_logic_vector(23 downto 16) is q(23 downto 16);
	alias register_output_3 : 
		std_logic_vector(31 downto 24) is q(31 downto 24);
begin

	resting_is_1 : if button_up_is_one = true generate
		actual_write_signal <= not write_signal;
	end generate resting_is_1;

	resting_is_0 : if button_up_is_one = false generate
		actual_write_signal <= write_signal;
	end generate resting_is_0;
	-- This will control which 8-bit register gets written to.
	-- Each 'write signal' is hooked up to the clock of an
	-- 8-bit register, so that the rising edge of the write signal
	-- causes a write to one of the 8-bit regions.
	write_demux: demux
	port map(
		input => actual_write_signal,
		selectors => region_selectors,
		outputs => write_signals
	);

	register_0: eight_bit_buffer
	port map(
	 load => '1',
	 clock => write_signals(0),
	 data => data,
	 q => register_output_0
 );

	register_1: eight_bit_buffer
	port map(
	 load => '1',
	 clock => write_signals(1),
	 data => data,
	 q => register_output_1
 );

	register_2: eight_bit_buffer
	port map(
	 load => '1',
	 clock => write_signals(2),
	 data => data,
	 q => register_output_2
 );

	register_3: eight_bit_buffer
	port map(
	 load => '1',
	 clock => write_signals(3),
	 data => data,
	 q => register_output_3
 );

end arch;

-- TODO:
-- - Implement LED controls so that user can insepct contents of 
--   registers on the board that they're using
-- - Implement LED passthrough, so that users can pass
--	 normal project controls into the buffer and have them show
--	 up on the board by switching to those controls.

-- DONE:
-- - Implement basic buffer. Just has controls.
