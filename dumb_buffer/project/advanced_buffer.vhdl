Library ieee;
use ieee.std_logic_1164.all;

entity advanced_buffer is
	generic(
		button_up_is_one : boolean := true;
		-- Passed to underlying simple buffer. See simple_buffer.vhdl for description.
		show_buffer_state: boolean := true;
		-- If true, then the state of the selected eight bit region
		-- will be displayed on the board LEDs.
		led_passthrough: boolean := true
		-- If you want to show the state of the buffer, you can choose to
		-- have the buffer multiplex your project's LED signals with the
		-- buffer state LED signals, rather than trying to handle that logic
		-- yourself. If you set this to true, and send in your project's
		-- LED signals to the `led_inputs` port, then the buffer can
		-- switch between the incoming signals to the `led_inputs` port and
		-- the state of the buffer.
	);
	port(
		write_signal : in std_logic := '0';
		-- Controls writing to the current eight bit region.
		-- Rising edge triggered.
		data : in std_logic_vector(7 downto 0) := (others => '0');
		-- Data to write to the current eight bit region.
		region_selectors : in std_logic_vector(1 downto 0);
		-- Selects eight bit region. Treat selectors as binary:
		-- - 00: 0th eight bit region (bits 0 through 7)
		-- - 01: 1th eight bit region (bits 8 through 15)
		-- - 10: 2th eight bit region (bits 16 through 23)
		-- - 11: 3th eight bit region (bits 24 through 31)
		-- led_inputs : in std_logic_vector(7 downto 0) := (others => '0');
		led_inputs : in std_logic_vector(7 downto 0) := X"00";
		-- Inputs to be passed through the buffer.
		led_output : out std_logic_vector(7 downto 0) := (others => '0');
		-- Outputs to the LEDs on the board. Normally shows the state
		-- of the currentlys selected 8-bit region.
		show_state_toggle : in std_logic := '0';
		-- Input for toggling whether or not currently selected
		-- byte is displayed on the LEDs. Is routed to the clock
		-- of a T Flip Flop where T is always 1.
		q : out std_logic_vector(31 downto 0) := (others => '0')
		-- The full output of the buffer. 32 bits wide (since there's four
		-- eight bit regions, 8*4=32 bits wide).
	);
end entity advanced_buffer;

architecture arch of advanced_buffer is

	component toggle_button 
		port(
			toggle : in std_logic := '0';
			state  : out std_logic := '0'
		);
	end component toggle_button;

	component simple_buffer 
		generic(
			button_up_is_one : boolean := true
		);
		port(
			write_signal     : in std_logic;
			data             : in std_logic_vector(7 downto 0);
			region_selectors : in std_logic_vector(1 downto 0);
			q                : out std_logic_vector(31 downto 0)
		);
	end component simple_buffer;


	signal buffer_output : std_logic_vector(31 downto 0);
	signal current_region : std_logic_vector(7 downto 0);
	signal show_state : std_logic := '0';
	-- Toggles displaying state of the registers.
	-- Shows when '1', otherwise shows the value of led_inputs.

	alias first_byte : 
		std_logic_vector(7 downto 0) is buffer_output(7 downto 0);
	alias second_byte : 
		std_logic_vector(15 downto 8) is buffer_output(15 downto 8);
	alias third_byte : 
		std_logic_vector(23 downto 16) is buffer_output(23 downto 16);
	alias fourth_byte : 
		std_logic_vector(31 downto 24) is buffer_output(31 downto 24);
begin

	underlying_buffer : simple_buffer
	generic map(
		button_up_is_one => button_up_is_one
	)
	port map(
		write_signal     => write_signal,
		data             => data,
		region_selectors => region_selectors,
		q => buffer_output
	);
	q <= buffer_output;

	toggle_switch : toggle_button
	port map(
		toggle => show_state_toggle,
		state => show_state
	);

	-- LED multiplexor. Selectors decide which 8-bit region is
	-- sent to the led_outputs.
	current_region <= first_byte when region_selectors = "00" else
		second_byte when region_selectors = "01" else
		third_byte when region_selectors = "10" else
		fourth_byte when region_selectors = "11";

	-- If show_buffer_state is false, then the led_outputs are never
	-- changed and so stay at their default of all zeroes.
	-- I don't try to pass signals when `show_buffer_state` is false
	-- because, in that case, I expect you to manipulate the pins
	-- directly yourself. The only reason that you'd activate this
	-- generic is so that the buffer can multiplex your lights for
	-- you, when you want to be able to bounce between seeing buffer
	-- countents and seeing project signals.
	LED_STATE_DISPLAY: if show_buffer_state = true generate
		NO_PASSTHROUGH: if led_passthrough = false generate
			led_output <= current_region;
		end generate NO_PASSTHROUGH;

		PASSTHROUGH: if led_passthrough = true generate
			led_output <= led_inputs when show_state = '0' else
										current_region when show_state = '1';
		end generate PASSTHROUGH;
	end generate LED_STATE_DISPLAY;

end arch;

-- TODO:
-- - Make the advanced features optional by use of generics.
--	 Based on the value of the generics, conditionally generate
--	 the statements for the LED functionality.

-- DONE:
-- - Implement basic buffer. Just has controls.
-- - Implement LED controls so that user can insepct contents of 
--   registers on the board that they're using
-- - Implement LED passthrough, so that users can pass
--	 normal project controls into the buffer and have them show
--	 up on the board by switching to those controls.
