LIBRARY ieee;
USE ieee.std_logic_1164.all;
use work.memory.T_flip_flop;

entity toggle_button is
	port(
		toggle : in std_logic := '0';
		state  : out std_logic := '0'
	);
end entity toggle_button;

architecture arch of toggle_button is
begin

	toggler : T_flip_flop
	port map(
		T => '1',
		CLK => toggle,
		q => state
	);

end arch;

-- Verified on board.
