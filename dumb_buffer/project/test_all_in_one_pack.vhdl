-- This is to see if having all pieces in one file is feasible. Editing might
-- suck, and it'll be more of a nightmare to upkeep, and version control will be
-- horrible, but I'm not terribly concerned about that. At least it'll be simple
-- to actually use, in lieu of real library support.

-- Not intended for practical use. Just intended to see if things would actually work.

Library ieee;
USE ieee.std_logic_1164.all;
use work.buffers.all;


entity all_in_one_pack_test is
	port (
		input     : in std_logic_vector(3 downto 0)  := X"0";
		selectors : in std_logic_vector(1 downto 0)  := "00";
		write : in std_logic := '0';
		output    : out std_logic_vector(7 downto 0) := X"00"
	);
end entity all_in_one_pack_test;

architecture arch of all_in_one_pack_test is
	signal first_buffer_input  : std_logic_vector(7 downto 0) := X"00";
	signal second_buffer_input : std_logic_vector(7 downto 0) := X"10";
	signal third_buffer_input  : std_logic_vector(7 downto 0) := X"20";
	signal fourth_buffer_input : std_logic_vector(7 downto 0) := X"30";

	signal first_buffer_output  : std_logic_vector(7 downto 0) := X"00";
	signal second_buffer_output : std_logic_vector(7 downto 0) := X"10";
	signal third_buffer_output  : std_logic_vector(7 downto 0) := X"20";
	signal fourth_buffer_output : std_logic_vector(7 downto 0) := X"30";
begin
	first_buffer_input(3 downto 0) <= input;
	second_buffer_input(3 downto 0) <= input;
	third_buffer_input(3 downto 0) <= input;
	fourth_buffer_input(3 downto 0) <= input;

	first_buffer : eight_bit_buffer
	port map(
		clock => write,
		load => '1',
		data => first_buffer_input,
		q => first_buffer_output
	);
	second_buffer : eight_bit_buffer
	port map(
		clock => write,
		load => '1',
		data => second_buffer_input,
		q => second_buffer_output
	);
	third_buffer : eight_bit_buffer
	port map(
		clock => write,
		load => '1',
		data => third_buffer_input,
		q => third_buffer_output
	);
	fourth_buffer : eight_bit_buffer
	port map(
		clock => write,
		load => '1',
		data => fourth_buffer_input,
		q => fourth_buffer_output
	);

	output <= first_buffer_output when selectors = "00" else
						second_buffer_output when selectors = "01" else
						third_buffer_output when selectors = "10" else
						fourth_buffer_output when selectors = "11";
end arch;
