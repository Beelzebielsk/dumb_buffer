LIBRARY ieee;
USE ieee.std_logic_1164.all;

-- Placing all the entities in one file isn't strictly necessary, nor
-- is placing everything into a package. However, it makes everything easy
-- for users to work with, though it makes modification a pain.
package buffers is
	component eight_bit_buffer
		port(
			clock : in std_logic;
			data  : in std_logic_vector (7 downto 0);
			load  : in std_logic;
			q     : out std_logic_vector (7 downto 0)
		);
	end component;

	component demux
		PORT (
			input     : in std_logic;
			selectors : in std_logic_vector(1 downto 0);
			outputs   : OUT STD_LOGIC_VECTOR (3 DOWNTO 0)
		);
	end component;

	component simple_buffer
		port(
			write_signal     : in std_logic;
			data             : in std_logic_vector(7 downto 0);
			region_selectors : in std_logic_vector(1 downto 0);
			q                : out std_logic_vector(31 downto 0)
		);
	end component;
end package buffers;
