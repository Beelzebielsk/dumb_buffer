LIBRARY ieee;
USE ieee.std_logic_1164.all;

-- Tested in modelsim. Works fine.
-- one input, four outputs (two selectors). In other words, a 1 to 4
-- demux.
ENTITY demux IS
	PORT (
		input     : in std_logic;
		selectors : in std_logic_vector(1 downto 0);
		outputs   : OUT STD_LOGIC_VECTOR (3 DOWNTO 0)
	);
END demux;

architecture arch of demux is
begin
	outputs(0) <= input and (not selectors(0)) and (not selectors(1));
	outputs(1) <= input and (selectors(0)) and (not selectors(1));
	outputs(2) <= input and (not selectors(0)) and (selectors(1));
	outputs(3) <= input and (selectors(0)) and (selectors(1));
end arch;
