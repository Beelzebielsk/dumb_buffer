Library ieee;
use ieee.std_logic_1164.all;

-- Used with new board. The pushbuttons are '1' when up.
-- In other words, they're active low.
entity button_signal_test is
	port(
		but_sig : in std_logic;
		led_sig : out std_logic
	);
end entity button_signal_test;

architecture arch of button_signal_test is
begin
	led_sig <= but_sig;
end arch;
