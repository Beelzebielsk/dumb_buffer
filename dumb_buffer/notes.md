
---
header-incldues:
	- \usepackage{float}
	- \usepackage{graphicx}
---

The project wizard seems pretty much the same.

Board name: 5CSEMA5F31C6

Version: 16.0.0.211

# Creating a Project

Most of it is the same.

\begin{figure}[H]
	\includegraphics{pictures/project_creation/000_new_project_manager_same.png}
	\caption{
		Start of Project Creation.
	}
\end{figure}

\begin{figure}[H]
	\includegraphics{pictures/project_creation/001_project_type.png}
	\caption{
		Start with an empty project. Don't use any templates. They
		might work, but you'll learn less, and if something goes wrong, you
		won't know if it's because of something you did, or some template
		setting.
	}
\end{figure}

\begin{figure}[H]
	\includegraphics{pictures/project_creation/002_dont_add_files_if_not_necessary.png}
	\caption{
		Just hit next unless you know what you're doing. Unless you know
		you have files that you want to add, there's no reason to use
		this.
	}
\end{figure}

\begin{figure}[H]
	\includegraphics{pictures/project_creation/003_device_setup.png}
	\caption{
		You need to use the device that you're using. This demo was done
		with the new DE1-SOC board. The name of the board is
		"5CSEMA5F31C6".  Type that into the \textbf{Name Filter} textbox.
	}
\end{figure}

\begin{figure}[H]
	\includegraphics{pictures/project_creation/004_default_tool_settings.png}
	\caption{Just accept the default settings.}
\end{figure}

\begin{figure}[H]
	\includegraphics{pictures/project_creation/005_accept_settings.png}
	\caption{Just hit finish.}
\end{figure}

# Using LPM Stuff

It's changed. It's no longer a separate wizard or manager that you run
when you want one thing. Instead, it's now under the *IP Catalog*.

The IP Catalog is a toolbar on the side. On this instance of *Quartus
Prime*, the IP Catalog toolbar is on the right side of the screen.

# Specifications of the Dumb Buffer

- Should allow for 32 inputs. 
- Should allow to switch between each set of eight inputs. 
- Should use a write enable, because if I don't, switching inputs will
	cause all the inputs in the new section to be overwritten by the
	current state of the switches. This isn't desirable.
- Should use the LEDs over the switches to show the current state of the
	inputs.
- Should give an *LED-passthrough*, which allows the user to switch
	between the LED output of their actual project and the LED output of
	the dumb buffer. Otherwise, they'd have to figure that all out
	themselves using mutliplexors, and they might not understand that. It
	will have inputs for the eight LEDs that it uses, and hooking up
	signals to those LEDs will cause those LEDs to light up as if the
	signal were sent right to the light. This should be a toggle, so there
	should be a T flip-flop.

## Arrangment of Board resources

There are 10 switches: these must all be used for having 32 inputs:
eight for actual input, four for selecting which set of eight inputs to
start using. So:

inputs[0]..inputs[7], sw[0]..sw[7]
selectors[0]..selectors[1], sw[8]..sw[9]

There should be a write enable, this will be set to a pushbutton. There
must also be a button to toggle which set of LEDs is shown: the buffer
LEDs or the project LEDs. 

write_enable, key[0]
led_toggle, key[1]

Perhaps as an optional thing, since there are only four pushbuttons, I
can make a pushbutton passthrough. It will give the first two
pushbuttons different functionality (project based functionality,
meaning the user just decides what they're for). This way you can switch
between using the buffer and doing things for the project. However, this
is less necessary than the led-passthrough, I'd argue. So this is an
optional step.

# How to make the buffer

On the right of the quartus screen should be the IP catalog. This is the
area with all the premade stuff. Look for the *LPM_SHIFTREG* device (
Library > Basic Functions > Miscellaneous > LPM_SHIFTREG). Click on it
twice to start making one.

Next, a small dialog will open. You'll be asked to give it a name. Give
it the name `eight_bit_buffer`, as that's what we'll be making. Big
picture, we'll make a 32 bit register out of 4 8 bit registers, and
we'll use a multiplexor and a decoder to control which register we write
to. Once, finished, hit 'OK'.

You should see the Megawizard Plug-In Manager. This is where you define
how specifics of the register that we'll be using: how wide is it, what
sorts of inputs and outputs should it have? Follow the settings given in
the pictures. After each step, hit the 'next' button and keep going
until the 'next' button is not usable, leaving only the 'finish' button.

In the final step, it gives you the option to select various output
files. You *must* check off "VHDL component declaration file". You can
also check off "Quartus Prime Symbol File" if you would like to build
the dumb register using

We have the following ports for the eight bit register:

- in: load, data, clock
- out: q

- data: The parallel input data.
- clock: The clock which controls the behavior of this digital circuit.
	Updates on the rising edge of the clock.
- load: Changes what the register does on the rising edge of the clock.
	Keep this '1' at all times.
	- When 0: The register shifts '1' into the serial input (which I shut
		off).
	- When 1: Reads input from the parallel input port (data).

# Testing the Buffer Out

To make a new waveform file, go to `File > New`, or press `CTRL+N`. Once
there, select `University Program VWF`. You will now have a file with
the extension `.vwf` (Vector Waveform File). We'll set the inputs to our
device in this file, and the outputs will be filled in accordingly once
we simulate.

It's possible that, once you create your waveform, you will have trouble
getting the thing to actually work. It will tell you that it could not
find a ModelSim executable. In this case, you have to change the
directory that quartus searches for the modelsim executable. On my
computer, this directory is `/opt/altera/16.0/modelsim_ase/bin`.

I've set the inputs to change at different times, all of them like
clocks (square waves which switch from off to on, or 0 to 1). The clock
has the highest frequency, the parallel input has the second highest
frequency, and the `load` input has the third highest frequency.

In the beginning the output of the shift register has '1' shifted into
it on every rising edge of the clock. So this is a rising edge device.
During this point in time, the `data` input is changing, but does not
affect the behavior of the devie, and the `load` input is constant at
`0`. Later on, after the `load` input switches to 1, the output of the
register follows the `data` input and updates every rising edge. So
`load` specifies whether or not to take in data from the parallel
inputs.

# Organizing the Project into a single package

To try and make things simple, I'd like to take everything and put it
into one file, rather than passing around a group of files and including
all the files.

I can place a bunch of different declarations in the same file, so I can
take all of the entities that I need, place them in one file, then
follow those entity declarations with the package declaration that I
want.

It turns out that the relative order of each component in a file doesn't
matter.  So components that a device depends on do not have to come
before the device itself, even though this helps clarity. So I can build
the project file by file to keep it simple and nice, then I can make a
makefile to concatenate all the pieces together in a certain order
(order not necessary, but students might want to read the file to
understand it), then publish that final file.

Couple of things to note:

- In general, dependencies don't matter.
- If, within a component, an *entity instatiation* is used (component is
	made without a component declaration, by using the statement `<label>
	: entity work.<component_name>`), order matters. When doing this, the
	compiler has to already be aware of the part mentioned. Component
	declarations allow for placeholders. So changing these things will be
	a real pain. Every change in an entity declaration has to be reflected
	in the component declarations of every device that uses it, and in the
	upper level package file. Even if I were to be able to force order
	using a makefile, the component declarations still have to make it to
	the final library, anyhow; using the makefile just moves the problem
	to another area as the dependencies still have to be made explicit.
- You can't compile a package directly (meaning as the top-level entity)
	in quartus, as there's no entity for it to create.

# Getting JTAG to work

1. Make sure that jtag is working correctly. Get driver files from arch
	 linux, run `jtagconfig` when board is plugged in and on to make sure
	 that non-error output shows.
2. Open the programmer window (`Tools > Programmer`)
3. Click `Auto detect`.
4. Two little pictures of chips should show up, with different names.
	 Click on the chip with the id number closer to the id number on the
	 ARM processor in the center of the board. For reference, that number
	 is `5CSEMA5F31C6N`.
5. Click on the change file button.
6. Find the `.sof` file in the output_files directory of your project,
	 then select that.
7. Finally, once that file has been loaded, check the
	 `program/configure` checkbox on the row that belongs to the chip you
	 selected in step 4.
