Demo of buffer with 8 bit comparator.

The board is configured:
- First eight switches are data switches.
- Last two switches are selector switches.
- Rightmost pushbutton is to write to the buffer.
- LEDs are controlled by the outputs of the comparator.

So, in general, the inputs from the board go to the buffer, the buffer
controls your actual project device that you built, and the outputs from
your device go to the outputs of the board, like LEDs or the seven
segment displays.

The buffer is hooked up to the comparator such that the first region of
the buffer is compared to the second region of the buffer.

Do little pattern.
