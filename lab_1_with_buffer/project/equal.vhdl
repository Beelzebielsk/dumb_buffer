Library ieee;
-- Load the ieee Library
use ieee.std_logic_1164.all;
-- Makes the stdlogic1164 pakcage
-- visible to everything in this file.

entity equal is
	port( i0, i1 : in std_logic;
			  eq : out std_logic);
end entity equal;

architecture arch of equal is
	signal both0, both1 : std_logic;
begin
	eq <= both0 or both1;
	both0 <= (not i0) and (not i1);
	both1 <= i0 and i1;
end arch;

