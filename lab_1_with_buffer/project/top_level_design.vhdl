library ieee;
use ieee.std_logic_1164.all;

-- The name of the buffer to use is 'simple_package'
-- You use it from the buffer package, which is called
--- 'buffers_package'.
use work.buffers.simple_buffer;

-- The behavior of this project is to demonstrate whether or not two eight
-- bit quantities are equal by using 8 one-bit comparators.
entity top_level_design is
	port(
		-- Inputs:
		-- The two selector switches. Choose which set of eight switches
		-- to manipulate.
		selectors : in std_logic_vector(1 downto 0);

		-- The data switches, which you use to actually enter data.
		data : in std_logic_vector(7 downto 0);

		-- A pushbutton of your choice. Used to write to the buffer.
		pushbutton : in std_logic; -- Pushbutton of your choice.
		
		-- The outputs are the outputs from your project:
		noteq, eq: out std_logic
	);
end entity top_level_design;

architecture arch of top_level_design is

	-- Your actual project becomes a component of this device.
	-- It's "inside" of it. It controls the behavior of the outputs
	-- and the buffer takes the board inputs.
	component eight_bit_comparator is
		port(
			i0, i1: in std_logic_vector(7 downto 0);
			noteq, eq : out std_logic
		);
	end component eight_bit_comparator;

	signal buffer_output : std_logic_vector(31 downto 0);
begin

	input_buffer : simple_buffer
	port map(
		write_signal     => pushbutton,
		data             => data,
		region_selectors => selectors,
		q                => buffer_output
	);

	-- The outputs from your project go directy to the
	-- outputs of this device.
	comparator : eight_bit_comparator
	port map(
		i0 => buffer_output(7 downto 0),
		i1 => buffer_output(15 downto 8),
		eq => eq,
		noteq => noteq
	);

end arch;
