Library ieee;
use ieee.std_logic_1164.all;

entity eight_bit_comparator is
	port(
		i0, i1: in std_logic_vector(7 downto 0);
		noteq, eq : out std_logic
	);
end entity eight_bit_comparator;

architecture arch of eight_bit_comparator is

	component equal is
		port( i0, i1 : in std_logic;
					eq : out std_logic);
	end component equal;

	signal equal_signals : std_logic_vector(7 downto 0);
begin

	equal_0 : equal
	port map(
		i0 => i0(0),
		i1 => i1(0),
		eq => equal_signals(0)
	);

	equal_1 : equal
	port map(
		i0 => i0(1),
		i1 => i1(1),
		eq => equal_signals(1)
	);

	equal_2 : equal
	port map(
		i0 => i0(2),
		i1 => i1(2),
		eq => equal_signals(2)
	);

	equal_3 : equal
	port map(
		i0 => i0(3),
		i1 => i1(3),
		eq => equal_signals(3)
	);

	equal_4 : equal
	port map(
		i0 => i0(4),
		i1 => i1(4),
		eq => equal_signals(4)
	);

	equal_5 : equal
	port map(
		i0 => i0(5),
		i1 => i1(5),
		eq => equal_signals(5)
	);

	equal_6 : equal
	port map(
		i0 => i0(6),
		i1 => i1(6),
		eq => equal_signals(6)
	);

	equal_7 : equal
	port map(
		i0 => i0(7),
		i1 => i1(7),
		eq => equal_signals(7)
	);

	eq <= equal_signals(0) and
				equal_signals(1) and
				equal_signals(2) and
				equal_signals(3) and
				equal_signals(4) and
				equal_signals(5) and
				equal_signals(6) and
				equal_signals(7);

	noteq <= not (equal_signals(0) and
								equal_signals(1) and
								equal_signals(2) and
								equal_signals(3) and
								equal_signals(4) and
								equal_signals(5) and
								equal_signals(6) and
								equal_signals(7));

end arch;
