---
header-includes:
	- \usepackage{tikz}
	- \usetikzlibrary{chains}
	- \usetikzlibrary{matrix}
	- \usetikzlibrary{decorations.pathreplacing}
	- \usetikzlibrary{calc}
	- \usepackage{verbatim}
	- \usepackage{float}
	- \directlua{ tex.enableprimitives('',tex.extraprimitives()) }
	- \newcommand{\calculate}[1]{\directlua{tex.print(#1)}}
geometry: margin=1in
---

# Intro

The board you're working with has 10 switches. You need more. This
project will give you more, though you'll have to get used to a few new
things.

You'll use a *buffer* to get more switches. In exchange for being able
to work with more inputs, you'll have to work on a small set of inputs
at a time, and switch between sets of inputs. 

The basic idea is to restrict yourself to a smaller set of switches than
normal-- in our case, 8 out of our 10 switches. You can set the switches
as you please, and the buffer will remember your choice of switches.
When you feel your choice of switches is good, you'll tell the buffer to
remember your choice. The unused switches are for moving to new regions
in the buffer, each of which can remember a different configuration of
switches. This way, you'll effectively have 32 switches to play with,
because you use 8 switches for data, and the remaining two switches to
choose between 4 regions in your buffer.

# How to Use

## Including in Your Device

To include in your device:

1. Download the package file from my github/bitbucket, wherever it'll
	 be. The packge is the `buffers` package.  TODO: fix this when
	 finished.
2. Copy the package file into your current project.
3. Add the file into your Quartus Project by going to `Project >
	 Add/Remove Files in Project`.
4. Once the package is in your project, open the VHDL file for the
	 device you're creating.
5. At the top of the file, where you have your `LIBRARY` and `USE`
	 declarations, add the following statement: `use
	 work.buffers.simple_buffer`. You will now be able to use the
	 `simple_buffer` device from the buffers package as if you had
	 declared it as a component. If you have not yet used a component,
	 then here's an example of how to use it:

~~~ {#mycode .vhdl}
	input_buffer : simple_buffer
	port map(
		write_signal     => pushbutton,
		data             => data,
		region_selectors => selectors,
		q                => buffer_output
	);
~~~~

This is a *component instantiation* in VHDL. You create a device that
was already designed and hook it up to the device that you're currently
designing. You do so through a *port map*. If each signal in VHDL is
like a wire, then you're hooking up wires into the inputs and outputs of
the component device, something like working with real circuits.

Each line with a `=>` hooks up a port from the component to a signal in
your device. The component signals are on the left, your device's
signals are on the right. The package comes with an example project
which shows an example of how to design using the buffer.

## Use on the Board

The first eight switches on the board.

# How to Build With

The best way to build your projects is modularly. This is for many
reasons, not least of which is that you may end up reusing these
devices for later projects. In that case, whether or not you are using
a buffer to interact with your project has nothing to do with how the
device you are building actually works. The logic of its behavior
doesn't change.

So build the device as if you really had 32 switches to use. When you
are done, you will make another device which takes inputs and sends them
to the buffer, and takes outputs from the outputs of your device.

The buffer looks like so:

\begin{figure}[H]
	\begin{center}
		\drawbuffer{}{}{0,31}
	\end{center}
	\caption{
		A representation of the buffer. It has four regions, each eight
		switches wide. You can manipulate one of these regions at a time,
		but not the whole thing. That is the sacrifice made to turn 10
		switches on the board into 32 switches in a buffer.
	}
\end{figure}

It has four regions, each with eight switches, for a total of 32
switches. Your project will take it's input from these 32 switches on
the buffer.

Just like normal switches, you can flip them on and off, and when you do
so, they will retain their state.

TODO: Get a picture of the small board for reference in the report.

To interact with this buffer, you'll organize the switches on your board
like so:

\begin{figure}[H]
	\begin{center}
		\begin{tikzpicture}[decoration={brace,amplitude=1mm}]
			\drawSwitchRow{10}{7}
			\coordinate (separator) at ( divider7 |- {$(bottom-left)-(0,2mm)$});
			\coordinate (right) at ($(bottom-right)-(0,2mm)$);
			\coordinate (left) at ($(bottom-left)-(0,2mm)$);
			\coordinate (right-label) at ($(separator)!.5!(right) - (0,2mm)$);
			\coordinate (left-label) at ($(separator)!.5!(left) - (0,2mm)$);
				
			\draw [decorate,decoration=mirror] (separator) -- (right);
			\draw [decorate,decoration=mirror] (left) -- (separator);
			\node [below] at (left-label) {Selectors};
			\node [below] at (right-label) {Data};
		\end{tikzpicture}
	\end{center}
	\caption{
		This is a representation of the 10 switches on the DE1-SoC
		board. The two switches on the right are {\it selector} switches.
		They choose which region of the buffer you interact with. The eight
		switches on the right are {\it data} switches. Moving those switches
		moves the switches on the buffer.
	}
\end{figure}

The two switches labelled *selectors* select which region you'll
interact with, and the switches labelled data manipulate the buffer
switches in that region.

\begin{figure}[H]
	\begin{center}
		\drawbuffer{}{}{}
	\end{center}
	\caption{The buffer.}
\end{figure}
